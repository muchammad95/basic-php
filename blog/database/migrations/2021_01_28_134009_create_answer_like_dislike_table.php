<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswerLikeDislikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_like_dislike', function (Blueprint $table) {
          $table->integer('poin');
          $table->unsignedBigInteger('jawaban_id');
          $table->foreign('jawaban_id')->references('id')->on('questions');
          $table->unsignedBigInteger('profile_id');
          $table->foreign('profile_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_like_dislike');
    }
}
