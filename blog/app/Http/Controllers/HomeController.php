<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //Halaman Home/Halaman Utama
    public function home(){
      return view('home');
    }
}
