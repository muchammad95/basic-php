<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //Halaman Register
    public function register(){
      return view('register');
    }

    //Halaman Welcome
    public function welcome(){
      $nama = 'Muchammad';
      return view('welcome', ["nama" => $nama]);
    }
}
