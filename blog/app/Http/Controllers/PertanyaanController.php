<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    //Membuat halaman index untuk menampilkan list data pertanyaan-pertanyaan
    public function index(){
      $post = DB::table('questions')->get();
      return view('index_pertanyaan', compact('post'));
    }

    //Membuat halaman create untuk 	menampilkan form untuk membuat pertanyaan baru
    public function create(){
      return view('create_pertanyaan');
    }

    //Membuat halaman store untuk menyimpan data baru ke tabel pertanyaan
    public function store(Request $request){
        $query = DB::table('questions')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan/create');
    }

    //Membuat halaman show untuk menampilkan detail pertanyaan dengan id tertentu
    public function show($id){
      $post = DB::table('questions')->where('id', $id)->first();
      return view('show_pertanyaan', compact('post'));
    }

    //Membuat halaman edit untuk menampilkan form untuk edit pertanyaan dengan id tertentu
    public function edit($id){
      $post = DB::table('questions')->where('id', $id)->first();
      return view('edit_pertanyaan', compact('post'));
    }

    //Membuat halaman update untuk menyimpan perubahan data pertanyaan (update) untuk id tertentu
    public function update($id, Request $request){
      $query = DB::table('questions')
            ->where('id', $id)
            ->update([
                'judul' => $request["judul"],
                'isi' => $request["isi"]
            ]);
        return redirect('/pertanyaan');
    }

    //Membuat halaman delete untuk menghapus pertanyaan dengan id tertentu
    public function destroy($id){
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
