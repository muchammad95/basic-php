@extends('adminlte.master')
@section('content')
<form role="form" action="/pertanyaan" method="POST">
  @csrf
  <h1>Membuat Pertanyaan</h1>
                  <div class="card-body">

                    <div class="form-group">
                      <label for="judul">Judul Pertanyaan</label>
                      <input type="text" class="form-control" id="title" name="judul" placeholder="Judul Pertanyaan">
                    </div>

                    <div class="form-group">
                      <label for="isi">Isi</label>
                      <input type="text" class="form-control" id="isi" name="isi" placeholder="Isi Pertanyaan">
                    </div>

                </div>

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                  </div>
</form>
<a href="/pertanyaan" class="btn btn-warning">List Pertanyaan</a>
@endsection
