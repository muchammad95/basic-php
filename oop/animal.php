<?php

class Animal{

  //Class Property
  protected   $name,
              $legs,
              $cold_blooded;

  public function __construct($name = "", $legs = 2, $cold_blooded = "false"){
  //Construct function
    $this -> name = $name;
    $this -> legs = $legs;
    $this -> cold_blooded = $cold_blooded;
  }

  //name Getter Function
  public function get_name(){
    return $this -> name;
  }

  //legs Getter Function
  public function get_legs(){
    return $this -> legs;
  }

  //cold_blooded Getter Function
  public function get_cold_blooded(){
    return $this -> cold_blooded;
  }
}

?>
