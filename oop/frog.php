<?php

#Load file animal.php
require_once "animal.php";

#Frog class -> extends animal class
class Frog extends Animal{

  public function __construct($name = "", $legs = 4, $cold_blooded = "false"){
  //Construct function
    $this -> name = $name;
    $this -> legs = $legs;
    $this -> cold_blooded = $cold_blooded;
  }

  public function get_legs(){
    return $this -> legs;
  }

  #jump method (only for Ape)
  public function jump() {
    return "hop hop";
  }

}

?>
