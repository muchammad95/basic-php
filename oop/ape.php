<?php

#Load file animal.php
require_once "animal.php";

#Ape class -> extends animal class
class Ape extends Animal{

  #Yell method (only for Ape)
  public function yell() {
    return "Auooooo~";
  }

}

?>
