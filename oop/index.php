<?php

#Load file from oop folder
spl_autoload_register(function($class){
  require_once $class . ".php";
});


//Release 0 -> class animal
$sheep = new Animal("shaun");

echo $sheep->get_name(); // "shaun"
echo "<br>";
echo $sheep->get_legs(); // 2
echo "<br>";
echo $sheep->get_cold_blooded(); //
echo "<br>";

//Release 1 -> class ape
$sungokong = new Ape("kera sakti");
echo $sungokong->yell(); // "Auooo"
echo "<br>";

//Release 1 -> class frog
$kodok = new Frog("buduk");
echo $kodok->jump(); // "hop hop"
echo "<br>";
echo $kodok->get_legs(); // tambahan aja, penasaran

?>
