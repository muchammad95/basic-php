<?php

function hitung_string($string_data){

  $string_split = str_split($string_data);

  for ($i = 0; $i < count($string_split); $i++){
    if ($string_split[$i] == "*"){
      return (int)explode("*",$string_data)[0]*(int)explode("*",$string_data)[1];
    }
    elseif ($string_split[$i] == "+") {
      return (int)explode("+",$string_data)[0]+(int)explode("+",$string_data)[1];
    }
    elseif ($string_split[$i] == "-") {
      return (int)explode("-",$string_data)[0]-(int)explode("-",$string_data)[1];
    }
    elseif ($string_split[$i] == ":") {
      return (int)explode(":",$string_data)[0]/(int)explode(":",$string_data)[1];
    }
    elseif ($string_split[$i] == "%") {
      return (int)explode("%",$string_data)[0]%(int)explode("%",$string_data)[1];
    }
  }

}

echo hitung_string("102*2");
echo "<br>";
echo hitung_string("2+3");
echo "<br>";
echo hitung_string("100:25");
echo "<br>";
echo hitung_string("10%2");
echo "<br>";
echo hitung_string("99-2");
echo "<br>";
echo "<hr>";

function perolehan_medali($array_medali){

  $emas_indonesia = 0;
  $perak_indonesia = 0;
  $perunggu_indonesia = 0;
  $emas_india = 0;
  $perak_india = 0;
  $perunggu_india = 0;
  $emas_korsel = 0;
  $perak_korsel = 0;
  $perunggu_korsel = 0;

  for ($i = 0; $i < count($array_medali); $i++){

      //Indonesia
      if(strtolower($array_medali[$i][0]) == "indonesia"){
        if(strtolower($array_medali[$i][1]) == "emas"){
          $emas_indonesia += 1;
        }
        elseif(strtolower($array_medali[$i][1]) == "perak"){
          $perak_indonesia += 1;
        }
        elseif(strtolower($array_medali[$i][1]) == "perunggu"){
          $perunggu_indonesia += 1;
        }
      }

      //India
      if(strtolower($array_medali[$i][0]) == "india"){
        if(strtolower($array_medali[$i][1]) == "emas"){
          $emas_india += 1;
        }
        elseif(strtolower($array_medali[$i][1]) == "perak"){
          $perak_india += 1;
        }
        elseif(strtolower($array_medali[$i][1]) == "perunggu"){
          $perunggu_india += 1;
        }
      }

      //Korea Selatan
      if(strtolower($array_medali[$i][0]) == "korea selatan"){
        if(strtolower($array_medali[$i][1]) == "emas"){
          $emas_korsel += 1;
        }
        elseif(strtolower($array_medali[$i][1]) == "perak"){
          $perak_korsel += 1;
        }
        elseif(strtolower($array_medali[$i][1]) == "perunggu"){
          $perunggu_korsel += 1;
        }
      }

  }

  $new_array = array(

    //Indoneia
    array(
      "negara" => "Indonesia",
      "emas" => $emas_indonesia,
      "perak" => $perak_indonesia,
      "perunggu" => $perunggu_indonesia,
    ),

    //India
    array(
      "negara" => "India",
      "emas" => $emas_india,
      "perak" => $perak_india,
      "perunggu" => $perunggu_india,
    ),

    //Korea Selatan
    array(
      "negara" => "Korea Selatan",
      "emas" => $emas_korsel,
      "perak" => $perak_korsel,
      "perunggu" => $perunggu_korsel,
    )

  );

  return $new_array;

}

print_r(perolehan_medali(
  array(
      array("Indonesia", "Emas"),
      array("India", "Perak"),
      array("Korea Selatan", "Emas"),
      array("India", "Perak"),
      array("India", "Emas"),
      array("Indonesia", "Perak"),
      array("Indonesia", "Emas")
  )
));

echo "<br><hr>";

$arr = [75,80,60];
$arr[]= 100;
print_r($arr);

?>
