<?php
function tentukan_nilai($number)
{
    //  kode disini
    if(!is_integer($number) || $number > 100 || $number < 0){
    	return "Tolong Masukkan Sebuah Bilangan Bulat 0 - 100";
    }
    else{
    	if(100 > $number && $number >= 85){
        	return "Sangat Baik <br>";
        }
        else if(85 > $number && $number >= 70){
        	return "Baik <br>";
        }
        else if(70 > $number && $number >= 60){
        	return "Cukup <br>";
        }
        else{
        	return "Kurang <br>";
        }

    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
